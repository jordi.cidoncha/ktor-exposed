package com.example.routes

import com.example.dao.DAOArticles
import com.example.dao.dao
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*

fun Route.articleRouting() {
    route("/articles") {
        get {
            val articleList = dao.allArticles()
            if (articleList.isNotEmpty()) {
                call.respond(articleList)
            } else {
                call.respondText("No articles found", status = HttpStatusCode.OK)
            }
        }
        get("{id?}") {
            val id = call.parameters["id"] ?: return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val article = dao.article(id.toInt()) ?: return@get call.respondText(
                    "No article with id $id",
                    status = HttpStatusCode.NotFound
                )
            call.respond(article)
        }
        post {
            val formParameters = call.receiveParameters()
            val title = formParameters.getOrFail("title")
            val body = formParameters.getOrFail("body")
            val article = dao.addNewArticle(title, body)
            call.respondRedirect("/articles/${article?.id}")
        }
        delete("{id?}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
            val article = dao.article(id.toInt()) ?: return@delete call.respondText(
                "No article with id $id",
                status = HttpStatusCode.NotFound
            )
            if (dao.deleteArticle(id.toInt())) {
                call.respondText("Customer removed correctly", status = HttpStatusCode.Accepted)
            }
        }


    }
}